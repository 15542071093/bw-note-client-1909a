/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { useState, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom"
// useState
const NotView = ({startIime}) => {
  console.log('render'); // 异步任务
  const [times, setTimes] = useState(startIime || 3)
  let timer = useRef(null);  
  // 组件状态发生变化要保留timer值，并且timer改变不会更新页面
  // 保存组件常量  
  // 获取组件实例，组件dom
  const navigate = useNavigate();
  useEffect(() => {
    console.log('componentDidMount')
    timer.current = setInterval(() => { // 异步任务
      console.log('setInterval', times);
      // 定时器异步任务取到最新状态值
      setTimes((times) => times - 1);
    }, 1000)
    return () => {  // componentWillUnmount
      console.log('componentWillUnmount')
      clearInterval(timer.current);
    }
  }, []) 
  // 第二参数传 [] 代替componentDidMount
  // 第二参数不传值 代替 componentDidMount componentDidUpdate
  useEffect(() => { // times更新完成之后执行
    if(times <= 0){
      hanldClick(); // 跳页面 
    }
  }, [times])
  // 代替componentDidMount 和 times属性更新完成之后 componentDidUpdate
  const hanldClick = () => {
    navigate('/', {
      replace: true
    });
  }
  return (
    <div><img src="https://img.ixintu.com/download/jpg/202001/b82774426a72f75468896a6b2117b789.jpg" />您的页面找不到啦，{times}秒自动跳转首页，<button onClick={hanldClick}>点击按钮跳转首页</button></div>
  )
}

export default NotView;