/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import BaseCard from "coms/baseCard"
import {recommendList, tagList} from "@/api/article"
import { useEffect } from "react"
import { observer } from "mobx-react-lite"
import { useStore } from "@/store/Provider"
const ArticleView = () => {
  const { articleModule } = useStore()  // 隐式调用StoreContext.Consumer 得到 Provider提供的value
  // console.log('values-------',values);
  useEffect(() => {
    articleModule.getNavData();
  },[])
  return (
    <div className="wrapper">
      <div className="leftWrapper">
        <div className={"nav"}>
          {
            articleModule.navData.map(item => {
              return <span key={item.id}>{item.label}</span>
            })
          }
        </div>
      </div>
      <div className={'rightWrapper'}>
        <BaseCard 
          title="推荐阅读" 
          request={async () => {
            const { data } = await recommendList();
            return {
              data
            }
          }}
        >
          {
            (item) => {
              return <p key={item.id}>{item.title}</p>
            }
          }
        </BaseCard>
        <BaseCard 
        title="文章标签" 
        request={async () => {
          const { data } = await tagList();
          return {
            data
          }
        }}
      >
        {
          (item) => {
            return <p key={item.id}>{item.label}</p>
          }
        }
      </BaseCard>
      </div>
    </div>
  )
}

export default observer(ArticleView); // 组件和仓库进行响应 仓库状态发生变化 驱动组件视图更新