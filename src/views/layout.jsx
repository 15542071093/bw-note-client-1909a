/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import BaseHeader from "coms/baseHeader"
import BaseFooter from "coms/baseFooter"
import { Outlet } from "react-router-dom"
const Layout = () => {
  return (
    <div className={"container header"}>
      <BaseHeader />
      <div className={"content"}>
        <div className={"wrapper"}>
          {/**二级路由 */}
          <Outlet />
        </div>
      </div>
      <BaseFooter />
    </div>
  )
}

export default Layout;