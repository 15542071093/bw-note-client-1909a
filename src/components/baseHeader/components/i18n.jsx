/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Menu, Dropdown } from 'antd';
import IconFont from "@/utils/iconFont"
import i18n from "i18next"
const I18nBtn = () => {
  const hanldeClickMenu = ({ key }) => {
    // console.log(i18n,key);
    //切换语言 找i18nnext 
    i18n.changeLanguage(key);
  }
  const menu = (
    <Menu
      onClick={hanldeClickMenu} 
      items={[
       {
          key: 'zh',
          label: '中文'
       },
       {
          key:'en',
          label: '英文'
       }
      ]}
    />
  )
  return (
    <Dropdown overlay={menu}>
      <IconFont type="icon-qiehuanyuyan"/>
    </Dropdown>
  )
}

export default I18nBtn;