/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { useState, useEffect } from "react"
import IconFont from "@/utils/iconFont"

const themes = [
  {
    icon: 'icon-ai250',
    className: 'light'
  },
  {
    icon: 'icon-yejing',
    className: 'dark'
  }
]

const ThemeBtn = () => {
  const [themeInex, setThemeIndex] = useState(() => {
    return new Date().getHours() > 6 && new Date().getHours() < 19 ? 0 : 1;
  });

  useEffect(() => {
    document.documentElement.className = themes[themeInex].className;
  },[themeInex]);

  const hanldClickThemeIcon = () => {
    setThemeIndex(val => Number(!val))
  };

  return (
    <IconFont type={themes[themeInex].icon} onClick={hanldClickThemeIcon} />
  )
}

export default ThemeBtn;