/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Link } from "react-router-dom"
import cls from "classnames"
import style from "./style.module.less"  
import ThemeBtn from "./components/theme"
import I18nBtn from "./components/i18n"
import { useTranslation } from "react-i18next"
// react-create-app 脚手架处理了module.less module.css module.scss样式相关文件

const BaseHeader = () => {
  const { t } = useTranslation(); // 切换语言时触发的hook
  return (
    <div className={cls('header', style.header)}>
      <div className="wrapper">
        <Link to="/">{t('文章')}</Link>
        <Link to="/archives">{t('归档')}</Link>
        <div>
          <ThemeBtn />
          <I18nBtn />
        </div>
      </div>
    </div>
  )
}

export default BaseHeader;


// provider consumer  React.createContext()