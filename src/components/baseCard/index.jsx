/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { Card } from 'antd';
import { useEffect, useState } from "react"
const BaseCard = ({title = "标题", children, request = () => {} , ...props}) => {  // $attrs
  const [loading, setLoading] = useState(true)
  const [list, setList] = useState([])
  useEffect(() => {   // 发请求
    const req = request();
    if(req instanceof Promise){
      req.then((res) => {
        setLoading(false);
        if(Array.isArray(res.data)){
          setList(res.data);
        } else {
          throw new Error('BaseCard request promise resolve 返回值格式不正确')
        }
      })
    } else {
      throw new Error('BaseCard request 返回值必须是一个promise对象')
    }
  },[])
  return (
    <Card title={title} {...props} loading={loading}>
      {
        list.length ? (
          list.map(item => {
            // 不同结构的子元素
            return children(item)
          })
        ) : (
          <p className={'emptyList'}>暂无数据</p>
        )
      }
    </Card>
  )
}


export default BaseCard;



