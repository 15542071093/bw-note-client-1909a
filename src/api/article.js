/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import axios from "@/utils/axios"

export const recommendList = (params = {}) => axios.get('/api/article/recommend', {
  params
})

export const tagList = () => axios.get('/api/tag')

export const getNavData = () => axios.get('/api/category')

export const getCommentList = (detailId, params) => axios.get('/api/comment/host/'+detailId, {
  params
})

export const getDetaildData = (id) => axios.get('/api/article/'+id);

// ajax 和 fetch 会产生跨域问题

// 解决跨域
// 1. 开发环境 webpack-dev-server proxy配置帮助我们实现反向代理解决跨域问题

// 2. cors 服务端设置允许跨域的headers。向浏览器返回数据  access-control-allow-origin:"*"