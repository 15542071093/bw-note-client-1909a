/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { lazy } from "react"  // 指令 在模块解析阶段执行 必须声明在文件顶部

const routerConfig = [
  {
    path: '/',
    component: lazy(() => import("@/views/layout")),
    children: [
      {
        path: "",
        meta: {
          title: '文章'
        },
        component: lazy(() => import('@/views/article'))
      },
      {
        path: "archives",
        meta: {
          title: '归档'
        },
        component: lazy(() => import('@/views/archives'))
      },
      {
        path: "article/:id",
        meta: {
          title: '文章详情'
        },
        component: lazy(() => import('@/views/articleDetail'))
      }
    ]
  },
  {
    path: '*',
    meta: {
      title: '八维创作平台'
    },
    component: lazy(() => import("@/views/404"))
  }
]


export default {
  mode: 'history',
  routes: routerConfig
};

// import() 方法在脚本运行阶段执行，返回值是promise对象