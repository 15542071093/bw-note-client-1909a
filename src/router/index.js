/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { BrowserRouter, Route, Routes, HashRouter } from "react-router-dom"
import { Suspense } from "react"
import BaseLoading from "coms/baseLoading"
import routeConfig from "./router.config"
import { useTranslation } from "react-i18next"

const BeforeEach = ({Views,meta}) => {  // 切换路由时执行，可以修改路由的一些配置
  const { t } = useTranslation();
  // console.log('BeforeEach----render', t);
  document.title = t(meta?.title || '八维创作平台');
  return <Views />
}

const renderRoutes = (routes) => {
  return routes.map(({path, component: Views, children = [], meta = {}}) => (
    <Route path={path} element={<BeforeEach Views={Views} meta={meta} to={path} />} key={path}>
      {
        children.length ? renderRoutes(children) : null
      }
    </Route>
  ))
}
const BaseRouter = () => <Suspense loading={<BaseLoading />}>
  <Routes>
    {
      renderRoutes(routeConfig.routes)
    }
  </Routes>
</Suspense>;

const Router = () => {
  return routeConfig.mode === 'history' ? (
    <BrowserRouter>
      <BaseRouter />
    </BrowserRouter>
  ) : (
    <HashRouter>
      <BaseRouter />
    </HashRouter>
  )
}

export default Router;

