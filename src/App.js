/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import Router from "@/router"
import Provide from "@/store/Provider"
import store from "@/store"

function App() {
  return (
    <Provide {...store}>
      <Router />
    </Provide>
  );
}

export default App;
