/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { makeAutoObservable } from "mobx"
import { getNavData } from "@/api/article"
// makeAutoObservable把对象实例中的属性和方法变成proxy对象监听属性和方法变化

class ArticleModule {
  navData = []; // 属性
  setNavData(data){
    this.navData = data;
  }
  async getNavData(){ // 什么触发 进入页面的组件的时候触发 action
    const { data } = await getNavData();
    this.setNavData(data);
  }
}



export default makeAutoObservable(new ArticleModule());