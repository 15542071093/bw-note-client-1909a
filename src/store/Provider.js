/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { createContext, useContext } from "react"

export const StoreContext = createContext();  // 跨组件通信 根元素向子元素或孙子元素提供数据

const Provide = ({children, ...props}) => {
  return (
    <StoreContext.Provider value={props}>  {/**提供数据的组件 */}
      {children}
    </StoreContext.Provider>
  )
} 

export const useStore = () => {
  const Store = useContext(StoreContext);
  return Store;
}

export default Provide;