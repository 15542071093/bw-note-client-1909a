/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import articleModule from "./modules/article"
import commentModule from "./modules/comment"

export default {
  articleModule,
  commentModule
}