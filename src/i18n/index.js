/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import zh from "./zh"
import en from "./en"
import i18n from "i18next";
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from "react-i18next";

// xhr   XML Http Request   ajax对象
// new XMLHttpRequest()
i18n
.use(LanguageDetector)
.use(initReactI18next)
.init({
  // the translations
  // (tip move them in a JSON file and import them,
  // or even better, manage them via a UI: https://react.i18next.com/guides/multiple-translation-files#manage-your-translations-with-a-management-gui)
  resources: { // 配置源文件
    en: {
      translation: en
    },
    zh: {
      translation: zh
    }
  },
  fallbackLng: "en",
  interpolation: {
    escapeValue: false // react already safes from xss => https://www.i18next.com/translation-function/interpolation#unescape
  }
})




