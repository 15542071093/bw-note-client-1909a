/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import dayjs from "dayjs"
import relativeTime from "dayjs/plugin/relativeTime"
import updateLocale from "dayjs/plugin/updateLocale"
import "dayjs/locale/zh-cn"
dayjs.locale('zh-cn')
dayjs.extend(updateLocale)
dayjs.extend(relativeTime)


export const getNumber = (min,max) => Math.floor(Math.random() * (max - min) + 1);

export const getTimes = (createTime) => `大约 ${dayjs(createTime).fromNow()}`;

export const formatTime = (time) => dayjs(time).toString()

export const formatHTMLAddCopyBtn = (html) => html.replace(/\<pre\>\<code/g, '<pre><span class="copy-code-btn">复制</span><code')