/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { createFromIconfontCN } from '@ant-design/icons';

const IconFont = createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_3429588_qhqzd8chy6q.js',
});

export default IconFont;