/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import axios from "axios";


const _http = axios.create({
  timeout: 10000,
  baseURL: process.env.REACT_APP_BASEURL
});


_http.interceptors.response.use((res) => {
  if(res.data){
    return Promise.resolve(res.data);
  }
  return Promise.resolve(res);
},(error) => { // 什么时候进到这个函数 这个函数要干什么
  return Promise.reject(error);
})

export default _http;