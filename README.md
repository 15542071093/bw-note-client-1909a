<!--
 * @Author: Yh
 * @LastEditors: Yh
-->
# 扩展create-react-app项目配置

1. 下载`@craco/craco`
2. 修改package.json
```json
"scripts": {
    "start": "craco start",
    "build": "craco build"
},
```
3. 通过`craco`来运行项目
4. 在项目根目录中添加`craco.config.js`配置文件
5. 修改配置
```js
// craco.config.js
const path = require('path');
const CracoLessPlugin = require('craco-less');  //支持less
// 全局注入less变量文件
const cracoPluginStyleResourcesLoader = require('craco-plugin-style-resources-loader'); 
module.exports = {
  webpack: {
    alias: {  // 别名
      "@": path.join(__dirname, 'src'),
      'coms': path.join(__dirname, 'src/components'),
      'api': path.join(__dirname, 'src/api'),
    },
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: { '@primary-color': '#1DA57A' },
            javascriptEnabled: true,
          },
        },
      },
    },
    {
      plugin: cracoPluginStyleResourcesLoader,
      options: {
        patterns: [
          path.join(__dirname, 'src/style/var.less')
        ],
        styleType: 'less'
      }
    },
  ]
}
```


### 解决跨域，配置代理


### 复习webpack loader plugin


### eslint preitter

eslint:可组装的JavaScript和JSX检查工具
if(true)console.log('1');

const a="a";


cross-env 用来兼容不同系统设置环境变量的

### husky

husky 工具用来管理git hooks，在特定时间内执行的函数

1. 下载

```
yarn add husky
```

2. install 自动生成.husky文件
在packagejson中添加install指令，运行该指令
```
"scripts": {
  "installhusky":"install husky"
}
```

3. 添加钩子函数，在commit之前执行

- pre-commit： 提交前使用eslint 做代码校验，校验通过提交，不通过就提交不了
```
npx husky add .husky/pre-commit "npm run lint"
```
- commit-msg: 检测提交信息的
feat 新增 ， upd 更新， fix 修复问题， docs 文档更新， style样式更新, build 构建

git commit -m "type: message"

  1. 下载
  ```
    yarn add @commitlint/cli @commitlint/config-conventional 
  ```
  2. 添加`commitlint.config.js`配置文件
  ```js
  module.exports = {
  extends: ['@com mitlint/config-conventional'],
    rules: {
        'type-enum': [2, 'always', [
            "feat", "fix", "docs", "style", "refactor", "perf", "test", "build", "ci", "chore", "revert"
        ]],
        'subject-full-stop': [0, 'never'],
        'subject-case': [0, 'never']
    }
  };
  ```
  3. 添加`scripts`指令
  ```json
  {
    "scripts":{
      "commitmsg": "commitlint -e $GIT_PARAMS"
    }
  }
  ```
  4. 添加钩子
  ```
  npx husky add .husky/commit-msg "npm run commitmsg"
  ```


  5. hooks 必须在函数根作用域下使用,因为hooks存在链表中,要保证指针正确指向下个hooks，保证组件状态更新时，通知所有hooks

  - `useState` 
  作用：在函数组件中定义状态,让函数组件拥有自己的状态
  语法：`const [curValue, setValue] = useState(initValue)`
  解释： `initValue` 状态初始值， `curValue`：当前状态值， `setValue`：设置状态变化的函数
  返回值：数组 [当前状态值, 设置状态的函数]
  当状态需要改变调用”设置状态的函数“ 函数组件会重新执行渲染，但是会保留上次更新的状态

  - `useEffect`
  作用：让函数组件拥有生命周期[特定时间内执行的函数]（副作用）的能力
  语法：`useEffect(() => {},[dep]?)`
  解释：`useEffect`第一个参数必须是一个函数， 第二个参数是选填参数
  第二参数不传：`useEffect(() => {})` 作用： componentDidMount componentDidUpdate
  第二参数不传：`useEffect(() => { return () => {} })` 作用： componentDidMount componentDidUpdate, 在componentWillUnMount的时候执行return返回的函数
  第二个参数传递空数组： `useEffect(() => { return () => {}},[])` 作用：componentDidMount, componentWillUnMount
  第二个参数传递数组，数组中有依赖项： `useEffect(() => { console.log(a) return () => {}},[a])` 作用：componentDidMount,shouldComponentUpdate 当a变化时返回true , componentDidUpdate, componentWillUnMount
  可以取到a状态的最新值

  - `useRef`
  作用：让函数组件拥有常量，组件状态更新,组件重新渲染，但是当前变量值保留 
       可以获取dom节点，获取组件实例
  对比：react.createRef(), 当元素或者组件实例发生变化，它不会追踪，但是useRef会追踪

  - `useContext`
  作用：`Context.Consumer`的语法糖






  <!-- function A(){
    console.log('render');
    const a1 = useRef(null);
    const [a, seta] = useState()
    useEffect(() => { // 特定时间内执行的函数 react在特定时间内去执行了这个函数
      getData(); // 只想在第一次调用该函数时执行
      a1.current = "a1";
    })
    seta()
  } -->